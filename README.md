# sortix

C++ kernel, monolithic, System V


Sortix is free software licensed under the ISC license and includes third party source code under different licenses and comes with no warranty whatsoever as described on the license page.

Sortix is a hobbyist operating system that lacks many features and you should not compare it with real operating systems. There is no networking, no cdrom driver, no USB driver, no SMP support, no power efficient idle (single core at constant 100% load), and such. See the list of limitations. These features will be added in future releases.

The installation and first boot process is covered in installation(7) that you should read ahead of time. Upgrading an existing installation is covered in upgrade(7). Basic usage for new users is covered in user-guide(7). See also the full manual. Be sure to read the system requirements below and the current operating system limitations. Read this whole page.

You can download the latest stable release of Sortix in the build/ subdirectory of these mirrors:

pub.sortix.org (Germany)
ahti.space (US)
All files are checksumed by the sha256sum file in the release directory. Ensure the checksum file matches across mirrors, then run the sha256sum tool to calculate the checksum of the files you downloaded and compare it against the official checksums. This authenticates the download assuming you can trust the sha256sum file. The sha256sum of sha256sum is 9b6288d4e0600167f1e7363f3cf852b332e66292cd8026f937366f46af6440c3. Sortix.org has https.

The system source code without ports (sortix-1.0.tar.xz) and with ports (sortix-1.0-full.tar.xz) can be found in the source/ subdirectory. The compiler toolchain used to build this release can be found in the toolchain/ subdirectory. The source code can also be browsed online.

You can download older releases by browsing to the parent directory (..) of the mirrors, but note that these are no longer supported.

System Requirements
32-bit x86 CPU with SSE (i686 release), or 64-bit x86 CPU (x86_64 release).
1 GiB RAM (recommended) to run iso live environment (including installer) with all ports loaded, or significantly less if unimportant ports are not loaded. An installation on a harddisk will require very little RAM to run.
ATA or AHCI harddisk with at least 1 GiB of unpartitioned space.
BIOS firmware, or UEFI firmware in legacy mode.
PS/2 keyboard/mouse firmware emulation to use those devices.
If you wish to dual boot, you need an existing operating system with a multiboot compliant bootloader such as GRUB.
CD boot
Download the appropriate .iso file and burn it to a physical medium. Boot it on the target machine and follow the instructions in installation(7).

USB boot
Sortix .iso releases are hybrid images that can be booted from USB portable storage. Assuming firmware support for this scheme, you can burn the release onto the USB. Find the name of the USB device, let's call that /dev/sdx, you can then run:

sudo dd if=sortix-1.0-x86_64-iso of=/dev/sdx bs=1M

The bs option should improve the transfer speed as dd defaults to 512 byte operations. After the transfer is complete, boot it on the target machine and follow the instructions in installation(7).

QEMU boot
Virtual machines such as Qemu and VirtualBox are well supported. To create a 1 GiB harddisk for the operating system, run:

qemu-img create sortix.raw 1G

To boot the 32-bit x86 release:

qemu-system-i386 -enable-kvm -m 1024 -vga std -cdrom sortix-1.0-i686.iso -drive file=sortix.raw,format=raw

To boot the 64-bit x86 release:

qemu-system-x86_64 -enable-kvm -m 1024 -vga std -cdrom sortix-1.0-x86_64.iso -drive file=sortix.raw,format=raw

The -enable-kvm option enables hardware virtualization. This requires the appropriate kernel driver to have been installed, enabled and that you have permission to use it. You may also need to enable hardware virtualization in your firmware. The virtual machine will be significantly slower without this.

The -m 1024 option provides the recommended 1 GiB of memory.

The -vga std option enables a graphics card with enhanced features that the bootloader and operating system can take advantage of to provide higher display resolutions.

The -cdrom option specifies the iso file to boot. This option can be removed after the installation is complete. Boot time may improve if you pass -drive file=sortix.iso,if=virtio,cached=off instead of the -cdrom option.

The -drive option specifies the harddisk to use. This along with with the qemu-img command can be left out if you simply wish to try the live environment.

For more information, read the qemu documentation.

Run the virtual machine and follow the instructions in installation(7).

